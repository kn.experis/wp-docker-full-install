#!/bin/bash

# Docker
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh

# Docker Compose
curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Run Docker Compose with Wordpress
docker-compose up -d
